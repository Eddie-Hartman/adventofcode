﻿using Helpers;

string[] input = InputHelper.ReadInput("puzzle4input.txt");

int count = 0;

bool checkString(int x, int y, bool? forward, bool? down)
{
    int currentX = x;
    int currentY = y;
    for (int i = 1; i < 4; i++)
    {
        if (forward == true)
        {
            currentX++;
        }
        else if (forward == false)
        {
            currentX--;
        }

        if (down == true)
        {
            currentY++;
        }
        else if (down == false)
        {
            currentY--;
        }

        if (input[currentY][currentX] != "XMAS"[i])
        {
            return false;
        }
    }

    return true;
}

for (int x = 0; x < input[0].Length; x++)
{
    for (int y = 0; y < input.Length; y++)
    {
        if (input[y][x] == 'X')
        {
            bool backwards = x > 2;
            bool forwards = x + 3 < input[0].Length;
            bool up = y > 2;
            bool down = y + 3 < input.Length;

            if (backwards && up)
            {
                if (checkString(x, y, false, false))
                {
                    count++;
                }
            }

            if (up)
            {
                if (checkString(x, y, null, false))
                {
                    count++;
                }
            }

            if (forwards && up)
            {
                if (checkString(x, y, true, false))
                {
                    count++;
                }
            }

            if (forwards)
            {
                if (checkString(x, y, true, null))
                {
                    count++;
                }
            }

            if (forwards && down)
            {
                if (checkString(x, y, true, true))
                {
                    count++;
                }
            }

            if (down)
            {
                if (checkString(x, y, null, true))
                {
                    count++;
                }
            }

            if (backwards && down)
            {
                if (checkString(x, y, false, true))
                {
                    count++;
                }
            }

            if (backwards)
            {
                if (checkString(x, y, false, null))
                {
                    count++;
                }
            }
        }
    }
}

Console.WriteLine(count);