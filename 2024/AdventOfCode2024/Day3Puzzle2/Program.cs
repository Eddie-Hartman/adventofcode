﻿using System.Text.RegularExpressions;
using Helpers;

string[] input = InputHelper.ReadInput("puzzle3input.txt");

string text = String.Join("\n", input);

string regex = "(mul\\()(\\d{1,3})(,)(\\d{1,3})(\\))";
string regex2 = "(do\\(\\))";
string regex3 = "(don't\\(\\))";

MatchCollection matches = Regex.Matches(text, regex);

MatchCollection doMatches = Regex.Matches(text, regex2);
MatchCollection dontMatches = Regex.Matches(text, regex3);

List<Match> allMatches = new();

allMatches.AddRange(matches);
allMatches.AddRange(doMatches);
allMatches.AddRange(dontMatches);

allMatches = allMatches.OrderBy(match => match.Index).ToList();

int total = 0;
bool doAdd = true;

foreach (Match match in allMatches)
{
    if (doAdd)
    {
        if (match.Groups[1].Value == "mul(")
        {
            int num1 = int.Parse(match.Groups[2].Value);
            int num2 = int.Parse(match.Groups[4].Value);
            
            total += num1 * num2;
        }
        else
        {
            if (match.Groups[0].Value == "don't()")
            {
                doAdd = false;
            }
        }
    }
    else
    {
        if (match.Groups[0].Value == "do()")
        {
            doAdd = true;
        }
    }
}

Console.WriteLine(total);
Console.ReadLine();