﻿namespace Helpers;

public static class InputHelper
{
    public static string[] ReadInput(string filename)
    {
        return File.ReadAllLines($"../../../../Inputs/{filename}");
    }
}