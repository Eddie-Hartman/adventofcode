﻿using Helpers;

string[] input = InputHelper.ReadInput("puzzle1input.txt");

List<int> left = new();
List<int> right = new();

void ParseInput(string line)
{
    string[] parts = line.Split("   ");
    left.Add(int.Parse(parts[0]));
    right.Add(int.Parse(parts[1]));
}

foreach (string line in input)
{
    ParseInput(line);
}

left.Sort();
right.Sort();

int distance = 0;

for (int i=0; i<left.Count; i++)
{
    distance += Math.Abs(left[i] - right[i]);
}

Console.WriteLine(distance);