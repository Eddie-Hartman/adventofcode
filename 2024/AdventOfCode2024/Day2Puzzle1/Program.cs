﻿using Helpers;

string[] input = InputHelper.ReadInput("puzzle2input.txt");

List<int[]> reports = new();

void ParseInput(string line)
{
    string[] parts = line.Split(" ");
    int[] newArray = Array.ConvertAll(parts, int.Parse);
    reports.Add(newArray);
}

foreach (string line in input)
{
    ParseInput(line);
}

int safeCount = 0;

foreach (int[] report in reports)
{
    bool safe = true;
    int previous = 0;
    for (int i = 1; i < report.Length; i++)
    {
        int distance = report[i] - report[i - 1];
        if (distance == 0 || Math.Abs(distance) > 3 || (previous > 0 && distance < 0) || (previous < 0 && distance > 0))
        {
            safe = false;
            break;
        }
        else
        {
            previous = report[i] - report[i - 1];
        }
    }
    
    if (safe)
    {
        safeCount++;
    }
}

Console.WriteLine(safeCount);