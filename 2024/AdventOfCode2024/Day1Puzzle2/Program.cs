﻿using Helpers;

string[] input = InputHelper.ReadInput("puzzle1input.txt");

List<int> left = new();
Dictionary<int, int> right = new();

void ParseInput(string line)
{
    string[] parts = line.Split("   ");
    left.Add(int.Parse(parts[0]));
    int rightNumber = int.Parse(parts[1]);
    if (!right.TryAdd(rightNumber, 1))
    {
        right[rightNumber]++;
    }
}

foreach (string line in input)
{
    ParseInput(line);
}

int score = 0;

foreach (int current in left)
{
    score += right.TryGetValue(current, out int rightValue) ? rightValue * current : 0;
}

Console.WriteLine(score);