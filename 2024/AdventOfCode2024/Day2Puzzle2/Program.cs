﻿using Helpers;

string[] input = InputHelper.ReadInput("puzzle2input.txt");

List<List<int>> reports = new();

void ParseInput(string line)
{
    string[] parts = line.Split(" ");
    List<int> newArray = new();
    foreach (string part in parts)
    {
        newArray.Add(int.Parse(part));
    }
    reports.Add(newArray);
}

foreach (string line in input)
{
    ParseInput(line);
}

int safeCount = 0;

foreach (List<int> report in reports)
{
    bool safe = true;
    // Going to take the exhaustive approach. If one is safe without the dampener, it'll be safe with it too.
    for (int d = 0; d < report.Count; d++)
    {
        safe = true;
        int previous = 0;
        List<int> temp = [..report];
        temp.RemoveAt(d);
        for (int i = 1; i < temp.Count; i++)
        {
            int distance = temp[i] - temp[i - 1];
            if (distance == 0 || Math.Abs(distance) > 3 || (previous > 0 && distance < 0) ||
                (previous < 0 && distance > 0))
            {
                safe = false;
                break;
            }
            else
            {
                previous = temp[i] - temp[i - 1];
            }
        }

        if (safe)
        {
            break;
        }
    }

    if (safe)
    {
        safeCount++;
    }
}

Console.WriteLine(safeCount);