﻿using System.Text.RegularExpressions;
using Helpers;

string[] input = InputHelper.ReadInput("puzzle3input.txt");

string text = String.Join("\n", input);

string regex = "(mul\\()(\\d{1,3})(,)(\\d{1,3})(\\))";

MatchCollection matches = Regex.Matches(text, regex);

int total = 0;

foreach (Match match in matches)
{
    int num1 = int.Parse(match.Groups[2].Value);
    int num2 = int.Parse(match.Groups[4].Value);
    
    total += num1 * num2;
}

Console.WriteLine(total);
Console.ReadLine();